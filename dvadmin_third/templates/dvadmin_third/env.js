function getAapUrl(){
	if (process.env.NODE_ENV === 'development') {
		return 'http://127.0.0.1:80/'
	} else {
		return window.location.href.split('?')[0].split('api/dvadmin_third/index/')[0]
	}
}
export const API_URL = getAapUrl() //后台根域名